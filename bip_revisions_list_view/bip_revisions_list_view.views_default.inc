<?php
/**
 * @file
 * bip_revisions_list_view.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function bip_revisions_list_view_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'node_revisions';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node_revision';
  $view->human_name = 'Lista wersji dokumentu';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Lista wersji dokumentu';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['use_more_text'] = 'więcej';
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['exposed_form']['options']['submit_button'] = 'Zastosuj';
  $handler->display->display_options['exposed_form']['options']['reset_button_label'] = 'Przywróć';
  $handler->display->display_options['exposed_form']['options']['exposed_sorts_label'] = 'Sortuj po';
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['style_plugin'] = 'views_accordion';
  $handler->display->display_options['style_options']['collapsible'] = 1;
  $handler->display->display_options['style_options']['row-start-open'] = 'none';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Relationship: Content revision: Zawartość */
  $handler->display->display_options['relationships']['vid']['id'] = 'vid';
  $handler->display->display_options['relationships']['vid']['table'] = 'node_revision';
  $handler->display->display_options['relationships']['vid']['field'] = 'vid';
  /* Pole: Content revision: Updated date */
  $handler->display->display_options['fields']['timestamp']['id'] = 'timestamp';
  $handler->display->display_options['fields']['timestamp']['table'] = 'node_revision';
  $handler->display->display_options['fields']['timestamp']['field'] = 'timestamp';
  $handler->display->display_options['fields']['timestamp']['exclude'] = TRUE;
  $handler->display->display_options['fields']['timestamp']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['timestamp']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['timestamp']['date_format'] = 'long';
  /* Pole: Content revision: Wpis do dziennika */
  $handler->display->display_options['fields']['log']['id'] = 'log';
  $handler->display->display_options['fields']['log']['table'] = 'node_revision';
  $handler->display->display_options['fields']['log']['field'] = 'log';
  $handler->display->display_options['fields']['log']['label'] = 'Powód zmiany';
  $handler->display->display_options['fields']['log']['exclude'] = TRUE;
  $handler->display->display_options['fields']['log']['empty'] = '<em>brak</em>';
  /* Pole: Global: Custom text */
  $handler->display->display_options['fields']['nothing']['id'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['table'] = 'views';
  $handler->display->display_options['fields']['nothing']['field'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['label'] = '';
  $handler->display->display_options['fields']['nothing']['alter']['text'] = 'Data modyfikacji: [timestamp], powód modyfikacji: [log]';
  $handler->display->display_options['fields']['nothing']['element_label_colon'] = FALSE;
  /* Pole: Content revision: Tytuł */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node_revision';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = 'Tytuł wersji dokumentu';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  /* Pole: Zawartość (historical data): Body */
  $handler->display->display_options['fields']['body-revision_id']['id'] = 'body-revision_id';
  $handler->display->display_options['fields']['body-revision_id']['table'] = 'field_revision_body';
  $handler->display->display_options['fields']['body-revision_id']['field'] = 'body-revision_id';
  $handler->display->display_options['fields']['body-revision_id']['label'] = 'Treść wersji dokumentu';
  $handler->display->display_options['fields']['body-revision_id']['empty'] = '<em>pusty dokument</em>';
  /* Pole: Zawartość (historical data): Data przygotowania dokumentu: */
  $handler->display->display_options['fields']['field_signed_date-revision_id']['id'] = 'field_signed_date-revision_id';
  $handler->display->display_options['fields']['field_signed_date-revision_id']['table'] = 'field_revision_field_signed_date';
  $handler->display->display_options['fields']['field_signed_date-revision_id']['field'] = 'field_signed_date-revision_id';
  $handler->display->display_options['fields']['field_signed_date-revision_id']['settings'] = array(
    'format_type' => 'long',
    'fromto' => 'both',
    'multiple_number' => '',
    'multiple_from' => '',
    'multiple_to' => '',
  );
  /* Pole: Zawartość (historical data): Dokument podpisany przez: */
  $handler->display->display_options['fields']['field_signed-revision_id']['id'] = 'field_signed-revision_id';
  $handler->display->display_options['fields']['field_signed-revision_id']['table'] = 'field_revision_field_signed';
  $handler->display->display_options['fields']['field_signed-revision_id']['field'] = 'field_signed-revision_id';
  /* Sort criterion: Content revision: Updated date */
  $handler->display->display_options['sorts']['timestamp']['id'] = 'timestamp';
  $handler->display->display_options['sorts']['timestamp']['table'] = 'node_revision';
  $handler->display->display_options['sorts']['timestamp']['field'] = 'timestamp';
  $handler->display->display_options['sorts']['timestamp']['order'] = 'DESC';
  /* Contextual filter: Content revision: Nid */
  $handler->display->display_options['arguments']['nid']['id'] = 'nid';
  $handler->display->display_options['arguments']['nid']['table'] = 'node_revision';
  $handler->display->display_options['arguments']['nid']['field'] = 'nid';
  $handler->display->display_options['arguments']['nid']['default_action'] = 'default';
  $handler->display->display_options['arguments']['nid']['exception']['title'] = 'Wszystko';
  $handler->display->display_options['arguments']['nid']['default_argument_type'] = 'node';
  $handler->display->display_options['arguments']['nid']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['nid']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['nid']['summary_options']['items_per_page'] = '25';

  /* Display: Block */
  $handler = $view->new_display('block', 'Block', 'block');
  $translatables['node_revisions'] = array(
    t('Master'),
    t('Lista wersji dokumentu'),
    t('więcej'),
    t('Zastosuj'),
    t('Przywróć'),
    t('Sortuj po'),
    t('Asc'),
    t('Desc'),
    t('Get the actual content from a content revision.'),
    t('Updated date'),
    t('Powód zmiany'),
    t('<em>brak</em>'),
    t('Data modyfikacji: [timestamp], powód modyfikacji: [log]'),
    t('Tytuł wersji dokumentu'),
    t('Treść wersji dokumentu'),
    t('<em>pusty dokument</em>'),
    t('Data przygotowania dokumentu:'),
    t('Dokument podpisany przez:'),
    t('Wszystko'),
    t('Block'),
  );
  $export['node_revisions'] = $view;

  return $export;
}
