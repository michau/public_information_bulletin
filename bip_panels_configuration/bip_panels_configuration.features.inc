<?php
/**
 * @file
 * bip_panels_configuration.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function bip_panels_configuration_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "page_manager" && $api == "pages_default") {
    return array("version" => "1");
  }
}
