<?php
/**
 * @file
 * bip_panels_configuration.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function bip_panels_configuration_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: management:admin/structure/panels
  $menu_links['management:admin/structure/panels'] = array(
    'menu_name' => 'management',
    'link_path' => 'admin/structure/panels',
    'router_path' => 'admin/structure/panels',
    'link_title' => 'Panels',
    'options' => array(
      'attributes' => array(
        'title' => 'Get a bird\'s eye view of items related to Panels.',
      ),
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'parent_path' => 'admin/structure',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Panels');


  return $menu_links;
}
