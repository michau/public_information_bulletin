<?php
/**
 * @file
 * bip_basic_page_configuration.features.ckeditor_profile.inc
 */

/**
 * Implements hook_ckeditor_profile_defaults().
 */
function bip_basic_page_configuration_ckeditor_profile_defaults() {
  $data = array(
    'CKEditor Global Profile' => array(
      'name' => 'CKEditor Global Profile',
      'settings' => array(),
      'input_formats' => array(),
    ),
  );
  return $data;
}
