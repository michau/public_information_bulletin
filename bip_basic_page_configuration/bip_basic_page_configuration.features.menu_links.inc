<?php
/**
 * @file
 * bip_basic_page_configuration.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function bip_basic_page_configuration_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: management:admin/config/content/ckeditor
  $menu_links['management:admin/config/content/ckeditor'] = array(
    'menu_name' => 'management',
    'link_path' => 'admin/config/content/ckeditor',
    'router_path' => 'admin/config/content/ckeditor',
    'link_title' => 'CKEditor',
    'options' => array(
      'attributes' => array(
        'title' => 'Configure the rich text editor.',
      ),
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'parent_path' => 'admin/config/content',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('CKEditor');


  return $menu_links;
}
