<?php
/**
 * @file
 * bip_basic_page_configuration.features.inc
 */

/**
 * Implements hook_node_info().
 */
function bip_basic_page_configuration_node_info() {
  $items = array(
    'page' => array(
      'name' => t('Strona'),
      'base' => 'node_content',
      'description' => t('Use <em>basic pages</em> for your static content, such as an \'About us\' page.'),
      'has_title' => '1',
      'title_label' => t('Tytuł'),
      'help' => '',
    ),
  );
  return $items;
}
